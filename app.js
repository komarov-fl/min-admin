
require('dotenv').config();
const express = require('express');
const path = require('path');

require('./utils/mongoose-connect');
const usersRoute = require('./routes/users');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', usersRoute);

app.listen(process.env.APPLICATION_PORT);
