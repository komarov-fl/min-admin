const router = require('express').Router();

const controller = require('../controllers/users');

router.get('/', controller.index);
module.exports = router;
