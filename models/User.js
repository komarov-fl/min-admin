const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
  registar: { type: String, index: true },
  foreignId: { type: String, index: true },
  email: {
    type: String, unique: true, sparse: true, trim: true,
  },
  referrer: { type: mongoose.Schema.Types.ObjectId, index: true },
  referalId: { type: String, unique: true },
  created: Date,
  updated: Date,
  isVoted: Boolean,
  voteCoordinates: {
    latitude: Number,
    longitude: Number,
  },
  privateKey: String,
  wallet: String,
  isRegistered: Boolean,
});

module.exports = mongoose.model('User', UserSchema);
