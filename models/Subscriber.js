const mongoose = require('mongoose');

const SubscriberSchema = mongoose.Schema({
  email: { type: String, unique: true },
  created: Date,
  updated: Date,
});

module.exports = mongoose.model('Subscriber', SubscriberSchema);
