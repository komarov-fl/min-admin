const User = require('../models/User');

const limit = 10;

module.exports.index = async (req, res) => {
  const totalUsers = await User.count();

  const page = parseInt(req.query.page, 10) || 1;
  const skip = (page - 1) * limit;

  const totalPages = Math.ceil(totalUsers / limit);
  const users = await User.find().skip(skip).limit(limit);

  const prevPage = page === 1 ? 1 : page - 1;

  const nextPage = page + 1;

  res.render('users/index', {
    title: 'Users',
    totalUsers,
    users,
    totalPages,
    page,
    prevPage,
    nextPage,
  });
};
